package profs.producthunt.api;

import profs.producthunt.Retro.Post;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by qwerty on 15.12.2017.
 */

public interface TechAPI {

    @GET("/v1/me/feed")
    Call<Post> getPost();

    @GET("/v1/me/feed")
    Call<Post> getPost(@Query("published") String pubs);

}
